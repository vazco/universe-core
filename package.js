Package.describe({
    summary: 'Contains collection on steroids. ( access / doc helpers / user helpers ) Many awesome utilities',
    name: 'vazco:universe-core',
    version: '1.6.7',
    git: 'https://github.com/vazco/universe-core.git'
});

Package.on_use(function (api) {
    api.versionsFrom(['METEOR@1.0.4']);
    var implyPackages = [
        'vazco:universe-utilities',
        'vazco:universe-collection',
        'vazco:universe-access'
    ];
    api.imply(implyPackages);
});
